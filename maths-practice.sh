#!/bin/bash
# This program asks X questions about addition/subtraction/multiplication of single digit numbers.

############ 

COUNTER=0

# Declare some Arrays:
declare -a A
declare -a B
declare -a USER_ANSWER
declare -a CORRECT_ANSWER

############

# Check that at least two arguments are given..
if [ $# -lt 2 ]; then
  echo
  echo "Usage : $0 <add|subtract|multiply> <number-of-questions>"
  echo
  exit 1
fi

# Check if operation is correct.

if [ -z "$1" ] ; then
  echo
  echo "Please provide operation: add subtract or multiply. Exiting ..."
  echo
  exit 1
else
  case "$1" in
    "add")
      OPERATOR="+" 
      DISPLAY_OPERATOR="+"
      ;;
    "subtract")
      OPERATOR="-" 
      DISPLAY_OPERATOR="-"
      ;;
    "multiply")
      OPERATOR="*" 
      DISPLAY_OPERATOR="X"
      ;;
    "divide")
      OPERATOR="/"
      DISPLAY_OPERATOR="/"
      ;;
    *)
      echo 
      echo "Please provide the operation as 'addition', 'subtration' or 'multiplication'. Exiting ..."
      echo
      exit 1
      ;;
  esac
fi


echo

if [ -z "$2" ]; then
  echo "Setting total number of questions to 10."
  echo "You can set a different number by passing it as a parameter."
  echo 
  # The Array index starts from 0, so 0-9 will be actually 10
  QUESTIONS=9
else
  # The Array index starts from 0, so 0 - N-1 will be actually N
  QUESTIONS=$(expr $2 - 1)
fi



function generateRandomNumber() {
  # This function takes one argument about the size (number of digits) of a number to return.
  if [ -z "$1" ]; then
    DIGITS=1
  else
    DIGITS=$1
  fi


  # There are many ways to generate random numbers in bash
  # cat /dev/urandom | tr -dc 0-9 | head -c 1
  # awk '{print rand()}' 
  # awk '{print srand() }'
  
  # $RANDOM is bash built in mechanism
  # we need just one digit
  RANDOM_NUMBER=$(echo $RANDOM | cut -c $DIGITS)
  echo $RANDOM_NUMBER
}


echo
SCORE=0
for COUNTER in $(seq 0 ${QUESTIONS}); do
  # find single digit random number for a, and assign it to A[$COUNTER]
  # find single digit random number for b, and assign it to B[$COUNTER]
  # can use awk, if bc does not provide random numbers.
  A[$COUNTER]=$(generateRandomNumber)
  B[$COUNTER]=$(generateRandomNumber)

  # It is important to write as $$ARRAY[$i]}  instead of $ARRAY[$i]

  # Note: the actual operator ( + - * / ) need to be escaped or enclosed in quotes- especially for * .
  #       That is because the shell tries to expand * to all files in present directory,
  #       and results in a problem. so I needed to escape / quote * . 
  #       I checked and it is safe to espace/quote all operators in the expr command.
  # Note: Only double quotes work for operator in expr command, nothing else.

  CORRECT_ANSWER[$COUNTER]=$(expr ${A[$COUNTER]} "${OPERATOR}" ${B[$COUNTER]})

  # echo "DEBUG: ${A[$COUNTER]}  ${OPERATOR}  ${B[$COUNTER]}  =  ${CORRECT_ANSWER[$COUNTER]}"
  echo -n -e "What is  ${A[$COUNTER]}  ${DISPLAY_OPERATOR}  ${B[$COUNTER]} ?  "
  read -t 5 USER_ANSWER[$COUNTER]

  # Sometimes the user does not provide an answer. In that case it is null.
  if [ -z "${USER_ANSWER[$COUNTER]}" ]; then
    echo
  fi
done

echo
echo "Checking answers. Showing incorrect ones ..."
echo
for COUNTER in $(seq 0 ${QUESTIONS}); do
  # Sometimes the user does not provide an answer. In that case it is null.
  if [ ! -z "${USER_ANSWER[$COUNTER]}" ]; then
    if [ ${USER_ANSWER[$COUNTER]} -ne ${CORRECT_ANSWER[$COUNTER]} ]; then 
      echo "${A[$COUNTER]}  ${DISPLAY_OPERATOR}  ${B[$COUNTER]}  =  ${CORRECT_ANSWER[$COUNTER]}   --------- NOT ${USER_ANSWER[$COUNTER]}"
    else
      SCORE=$(expr $SCORE + 1)
    fi
  else
    # The answer was null, and a mathematical operation cannot be performed. Consider this wrong answer.
    echo "${A[$COUNTER]}  ${DISPLAY_OPERATOR}  ${B[$COUNTER]}  =  ${CORRECT_ANSWER[$COUNTER]}   --------- NOT (null or empty)"
  fi
done


# also show total score.
echo 
echo "--------------------------------------------------------------------------"
echo
echo "Your score: $SCORE / $(expr $COUNTER + 1) ."
echo
